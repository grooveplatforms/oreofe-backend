function isAuthenticated(req, res, next) {

    if (authenticateUser(req))
        return next();

    res.status(401).json({"message" : "Request Unauthorized"});
}

const authenticateUser = (req) => {
    return req.header('Authorization');
}

module.exports = exports = isAuthenticated;