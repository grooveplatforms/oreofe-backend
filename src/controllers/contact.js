/**
 * A simple CRUD controller for contacts
 * Create the necessary controller methods 
 */

import Contact from '../entities/contact';

export default {
    // get all contacts for a user
    all : ({res}) => {

        Contact.find({}).exec((err, docs) => {
            return res.json({"message" : "Request Successful", "body" : { "contacts" : docs.toJSON()}})
        });

        },
    // get a single contact
    get:  (req, res) => {

        Contact.find({ id: req.params.id }).exec((err, docs) => {
            return res.json({"message" : "Request Successful", "body" : { "contacts" : docs.toJSON()}})
        });

    },
    // create a single contact
    create: (req, res) => {

        //TODO:
        const contact = {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            address: req.body.address
        };

       // return res.json({"message" : "Contact saved"});

        Contact.create(contact, function (err) {
            if (err) return res.json(500, {error: err});

            return res.json({"message" : "Contact saved"});
        });


    },
    // update a single contact
    update: (req, res) => {
        Contact.updateOne({ id: req.params.id }).exec((err, docs) => {
            return res.json({"message" : "Request Successful"});
        });
    },
    // remove a single contact
    remove: (req, res) => {

        Contact.deleteOne({ id: req.params.id }).exec((err, docs) => {
            return res.json({"message" : "Request Successful"});
        });
    }
}