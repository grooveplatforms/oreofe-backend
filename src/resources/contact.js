import Contact from '../controllers/contact';
/**
 * 
 * 
 */
module.exports = app => {
    app.route('/contact/all').get(Contact.all);
    app.route('/contact').post(Contact.create);
    app.route('/contact/:id').get(Contact.get);
    app.route('/contact/:id').put(Contact.update);
    app.route('/contact/:id').delete(Contact.remove);
    /**
     * Create the remaining routes
     * get,
     * create,
     * delete,
     * update,
     * remove
     */
};
