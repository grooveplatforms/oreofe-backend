import mongoose, { Schema } from 'mongoose';

export const ContactSchema = new Schema(
    {
        //  define the necessary fields for your contact list
        email: {
            type: String,
            lowercase: true,
            trim: true,
            required: true
        },
        phone: {
            type: String,
            trim: true,
            required: true
        },
        address: {
            type: String,
            required: true,
        },
        name: {
            type: String,
            trim: true,
            required: true
        }
    },
    { collection: 'contacts' }
);

export default {
    get: () => {},
    getAll: () => {},
    update: () => {},
}

// mongoose.model('Contact', ContactSchema);
